The following text is extracted (copy/pasted and edited) from the [Design and fabrication of a Czeny-Turner monochromator-cum-spectrograph](https://inis.iaea.org/collection/NCLCollectionStore/_Public/19/019/19019134.pdf?r=1&r=1) by M.V.R.K. Murty, R.P. Shukla, S.S. Bhattacharya and G. Krishnamurty Spectroscopy Division. It acts as a short summary. Please refer to the document for more details.

#### Design of a Czerny-Turner monochromator

Schematic diagram: 

![monochromator schematic diagram](../images/monochromator_diagram.jpg)


It consists of a classically ruled grating having 1200 grooves/mm. The collimator is a concave spherical mirror having a radius of curvature 1.025 metre while the focusing element is a concave spherical mirror of radius of curvature 0.925 metre. 
The design of two unequal radii of curvature for collimating and focusing mirrors is chosen to eliminate the comatic aberration at the wavelength of 5000 Å. 
The linear reciprocal dipersion on the focal surface is about 8 Å/mm. The resolution of the instrument at the coma corrected wavelength i.e. 5000 Å is 0.1 Å. The resolution at the other wavelengths is limited by the residual comatic aberration which increases linearly with wavelength on either side of the 5000 Å. Therefore the resolution at the wavelength 2000 Å and 8000 Å is about 0.2 Å.  

We have chosen the design of Czerny-Turner type of monochromator useful for the wavelength range of 2000 Å to 10000 Å.  

A simple sine drive mechanism is incorporated for rotating the grating in order to cover the spectral region from UV to near IR. Different wavelengths are obtained by rotating the grating by a sine bar mechanism.  

Spherical and coma aberration have been minimized.  

In this scheme of coma correction, the two concave mirrors of unequal radii of curvature are used. We have chosen the zero coma point corresponding to 5000 Å.

The design data are summerized below :  
  
- Focal length of the collimating mirror = 1.025 metre  
- Diameter of the collimating mirror = 150 mm  
-   Focal length of the focusing mirror = 0.925 metre  
-   Diameter of the focusing mirror = 150 mm  
-   Grating ruled area = 102 mm X 102 mm  
-   Frequency of the grating = 1200 grooves / mm  
-   Angle (2Φ) between the incidence and diffracted beams=18°  
-   Off-axis angle (a) of the mirror M<sub>1</sub> = 4.5°  
-   Off-axis angle (b) of the mirror M<sub>2</sub> = 5°  
-   (α<sub>0</sub> ,β<sub>0</sub> ) are angles of incidence and diffraction respectively at which coma is zero corresponding to wavelength 5000 Å = 8°41' and 26°41'  
-   Spectrum width in one span = 1000 Å  
-   Reciprocal linear dispersion = 8 Å / mm (First order)  
-   Resolution of the spectrograph = 0.2 Å  
-   Lever length of the sine drive mechanism = 164.61 mm  
-   Pitch of the screw used for rotating the grating = 1 mm  
-   Wavelength shift for 1 mm movement of the screw = 100 Å  
-   Halfwidth of the monochromator = 0.2 Å  

In this method of coma correction at wavelength 5000 Å, we keep the symmetry of the entrance and exit slits. However the tangential coma (TA'<sub>coma</sub>) is not zero at other wavelengths. (TA'<sub>coma</sub>) is computed for various wavelengths and is plotted in the following figure. 

![coma plot](../images/coma_plot.jpg)
[also it might be here, maybe elsewhere but redo my calculation that works well]

We observe that the coma varies linearly with wavelength. The TA'<sub>coma</sub> is less than 0.028 mm over the entire range from 2000 Å to 8000 Å. 
The dispersion of the instrument with above parameters is about 8 Å/mm. Hence the coma spread is equivalent to 0.028 X 8 = 0.22 Å. Thus the half width spread of the output of the monochroraator may be made 0.2 Å over the entire range of 2000-8000 Å.  

#### SINE DRIVE MECHANISM

In the monochromators, the wavelength is slightly nonlinear with the angle of rotation of the grating. Sine drive is used extensively for linearizing the wavelength scale. A schematic illustration of the sine bar mechanism is shown in the following figure.  

![sine bar mechanism](../images/sine_bar_mechanism.jpg)

A lever of constant length L when rotated through an angle θ about one end, the other end moves through a distance L.Sin(θ) perpendicular
to the initial position of the lever.  

The grating equation is given by :
`λ = (2d Cos(Φ)) Sin(θ)`
and the displacement is given by :
`X = L Sin(θ)`
From these two equations we have
`λ = (2d Cos(Φ) x)/L`

Thus the wavelength is proportional to the linear displacement of the centre of the ball measured in a direction perpendicular to the initial position of the lever. 
The ball can be pushed by a nut actuated by a screw and the drum attached to the screw may be linearly calibrated in terms of wavelength. 
The screw is also coupled to a counter to read the wavelength.
A lead screw of 1.0 mm pitch was chosen for the ease in fabrication.
The lever length was fixed at 164.61 mm to give a wavelength shift of 100 Å by a screw movement of 1.0 mm as calculated from the last equation. 
The total length of the screw was calculated to be 100.0 mm to cover a spectral range of 0.0 Å to 10000 Å.

[edit stuff out. Also this is where I did my calculation and found a more exact value. Write it down here or above.]
#### Dispersion and resolution of the monochromator

A bandwidth of 0.1 Å can be obtained at the exit slit of width 13.5 micrometers.

The width of the exit slit sets a limit on the wavelength spread emerging out of the exit slit.  

Since we have corrected the coma for the wavelength λ = 5000 Å for which α<sub>0</sub> = 8°41', β<sub>0</sub> = 26° 41', we can obtain the computed bandwidth of 0.1 Å at this wavelength only, but for other wavelengths, the bandwidth will be limited by the bandwidth spread due to coma. It has been shown previously that the (TA'<sub>coma</sub>) for 2000 Å & 8000 Å is 0.028 mm which corresponds to a bandwidth spread of 0.2 Å. Hence we may set the halfwidth of the output of the monochromator as 0.2 Å over the full range of 2000 Å-8000 Å.
