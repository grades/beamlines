# Main result and status  
## Result  

The imaging branch has been simulated. For ease of implementation, a different monochromator from the one DISCO has has been simulated. The [following capture](#monochromator) showing the graph of the Wavelength(Å) as a function of x<sub>screw</sub>(mm) (more detail in the [monochromator paragraph](#monochromator)) shows the monochromator works correctly.  

The command to get that result is: `mxrun SOLEIL_DISCO.instr -N84 x_screw=20,103 scan=1`

The remaing objective now is to simulate the microlens array DISCO has and to simulate the other branches.

In the future, when fluorescence can be simulated with McXtrace, the fluorescence microscope can be simulated for the imaging branch.
The circular dichroism branch needs circular polarisation to be simulated within McXtrace, this will also take some work. Two issues have been added to the McXtrace github concerning [fluorescence](https://github.com/McStasMcXtrace/McCode/issues/1261) and [circular polarisation](https://github.com/McStasMcXtrace/McCode/issues/1285).  
McXtrace version 1.7 was used.
## Status  

- [x] Simulate imaging branch
- [ ] Simulate microlens array
- [ ] Simulate other branches

# The DISCO beamline

The DISCO beamline is divided into 3 parts (Fig 1) :

1 / an imaging branche (CX3 see the drawing below) working from 1.2 eV to 6.9 eV

2 / two Circular Dichroism (CD) sub branches (CX1 and CX2) delivering beam from 2 eV to 9.5 eV

For a detailed reading please refer to the article [1] below : 

[1] : https://www.researchgate.net/publication/38024993_DISCO_a_low-energy_multipurpose_beamline_at_synchrotron_SOLEIL

For information about this article we draw attention to the fact that the CX2 branch (APEX) was dedicated in the past to mass spectrometry and is replaced today by a second CD branch.


# Synopsis of the Disco beamline.  


| ![DISCO beamline image](images/Disco.png) | 
|:--:| 
| *DISCO synopsis* |


# 
# beamline devices table

| Optical element | Branch | Optical property | Distance from source (mm) | Grazing incidence | Material | Radius of curvature |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| M1 | CX1,2,3 | Cylindrical convex | 5800 | 22.5° | Si | r = -75 m |
| M2 | CX1,2,3 | Toroidal | 6000 | 22.5° | Si | R = 21.04, r = 2.56m |
| M3 | CX1,2,3 | Elliptic cylindrical | 16600 | 22.5° | Si | R = 9.93 m |
| M4 | CX1,2,3 | Plane | 16800 | 22.5° | Si | - |
| M51 | CX1,2,3 | Sphere convex | 19950 | 22.5° | Si | R = -35 m |
| M52 | CX3 | Cylindrical | 20750 | 20.65° | AlMgF2 | r = 2 m |
| M53 | CX3 | Toroidal | 44500 | 22.5° | AlMgF2 | R = 4.75 m, r = 0.714 m |
| M54 | CX3 | Toroidal | 44750 | 22.5° | AlMgF2 | R = 7.80 m, r = 1.12 m |
| M6A | CX1 | Toroidal | 28556 | 75° | Ru | R = 7.87 m, r = 5.56 m |
| M6B | CX2 | Spherical | 29075 | 15° | Si | R = 35.4 m|
| M7 | CX2 | Cylindrical | 32075 | 11° | Si | R = 37.85 m |
| GA | CX1 | Plane (ie lamellar), 400 lines mm-1 (vls) - groove/Period = 0.5 | 28729 | 75° | Ru | - |
| GB | CX2 | Plane (ie lamellar), 250 lines mm-1 (vls) - groove/Period = 0.5  | 28729 | 15° | Si | - |


# The source

Disco is equiped with a bending-magnet :
- magnet : 1.71 T
- bending angle : 11.25°

An aperture of a crotch absorber is located at 1.5 m from the bending magnet giving a beam divergence of 50 mrad horizontally by 10 mrad vertically. The crotch is equipped with a cold finger whose aim is to stop the higher energies located vertically into the center of the beam so as to let pass only the lower energies in a 1eV - 20 eV range [1].




# 1 : Imaging branch (CX3)

The imaging branch is equipped with an iHR320 Czerny Turner spectrometer (Fig 2) from Horiba [2]. According to the spectral working range of this branch, the spectrometer is equiped with a 100 lines/mm blazed grating. The entire optical chain of the CX3 branch focuses the beam at the input of the spectrometer. See [2], [3] and [4].


| ![Czerny-Turner spectrometer](images/Czerny-Turner-Configuration.png) | 
|:--:| 
| *Czerny-Turner spectrometer* |

[2] Horiba : https://static.horiba.com/fileadmin/Horiba/Products/Scientific/Optical_Components_and_OEM/iHR_Series/iHR_Series_Brochure.pdf

[3] Monochromator Czerny Turner configuration : https://www.stellarnet.us/what-is-a-czerny-turner-configuration/

[4] Czerny Turner : https://www.mdpi.com/2673-3269/3/1/1/htm 

# 2 : Circular Dichroism branch (CX1 & CX2)

CX1 use a classic set including in the order : the monochromator (GA see table above), a modulator converting alternatively linear polarisation into left and right circular polarisation, the sample to study and a photo multiplier collecting the signal. A locking amplifier synchronize the modulator and the photo multiplier in order to collect alternatively the right or left polarised signal which is then sampled via an ADC.


| ![CD branch](images/CD_classique_schema_de_principe.png) | 
|:--:| 
| *Circular Dichroisme aquisition scheme* |

CX2

## Simulation
### Monochromator

As explained above, the DISCO monochromator is made by HORIBA. Because some details were not findable even after requesting them directly to HORIBA (such as the two radiuses of each toroidal mirrors), it was decicded to use an open design of a Czerny-Turner monochromator.

This is the chosen design: [Design and fabrication of a Czeny-Turner monochromator-cum-spectrograph](https://inis.iaea.org/collection/NCLCollectionStore/_Public/19/019/19019134.pdf?r=1&r=1) by M.V.R.K. Murty, R.P. Shukla, S.S. Bhattacharya and G. Krishnamurty Spectroscopy Division.

The simulation done with McXtrace for a scan between 2000 and 10300 Angstroms (approximately 1.2 to 6.2 eV, the range of our source in the simulation) gives us this result: 

| ![scan monochromator](images/scan_monochromator.jpg) | 
|:--:| 
| *Graph of the Wavelength(Angstroms) as a function of screw length(mm)* |

The x-axis is the screw length(mm). See the "_Sine drive mechanism_" paragraph below for more details but put succintly when screw length x<sub>screw</sub>=20mm, λ=2000 Angstroms. And when x<sub>screw</sub>=103mm, λ=10300 Angstroms. When the screw length changes, the grating rotates and the wavelength of the detected light changes. See the following [one](https://www.youtube.com/watch?v=IVHPnBeMNYU) and [two](https://www.youtube.com/watch?v=1pIjSuK23RM) videos illustrating the rotation of the grating changing the light being selected. 
The simulation data can be found in the [simulation result folder](simulation_results).

The text in the subfolder [monochromator](doc_monochromator) is extracted (copy/pasted and edited) from the document, it acts as a short summary. Please refer to the document for more details.


## Optical elements to be implemented

### Microlens array

The document explaining the microlens array [here](https://www.suss-microoptics.com/suss-microoptics/technical-publications/SPIE_7102_19-%20Optical%20Design%20Conf.-%20Laser%20Beam%20Homogenizing-%20Glasgow%202008.pdf).

### Visible detector

This detector would show us the colours the monochromator selects in the visible range. 
Not important and optional.

## Objectives to be done

### Compare simulation results with beamline diagnostics

Compare the signal of the beamline's diagnostic monitors with the simulated beamline diagnostic monitors. 

### Compare results of the monochromator on the imaging branch

Compare the signal after and just before the monochromator on the imaging branch. 

### Take M51 out and see the consequence

The imaging branch gets 25% of the beam deviated to it from the M51 mirror, and the circular dichroism branch gets the remaining 75%. Check that when the mirror M51 is taken out, the dichroism branch get 100% of the signal.




