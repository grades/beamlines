### cif2hkl

The Si.lau file is generated with [cif2hkl](https://gitlab.com/soleil-data-treatment/soleil-software-projects/cif2hkl).  

It takes as input the cif file and spits out the hkl (.lau) file.  

Run the command:  
`./cif2hkl --xtal --mode XRA  --lambda 0.3099605 2104737.cif`

The lambda parameter is used to indicate the energy goes up to 40 keV.
