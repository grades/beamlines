# Simulating glitches with McXtrace

[Glitch runner SSRL BL 11-2](https://github.com/antoinepado/glitch_runner_ssrl)  
[Direct link to the Jupyter notebook](https://github.com/antoinepado/glitch_runner_ssrl/blob/master/glitch_runner_ssrl_bl11-2.ipynb)  

[Glitch runner SOLEIL ROCK](https://github.com/antoinepado/glitch_runner_rock)  
[Direct link to the Jupyter notebook](https://github.com/antoinepado/glitch_runner_rock/blob/master/glitch_runner_soleil_rock.ipynb)  
