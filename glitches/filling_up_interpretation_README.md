# Interpretation

When the source is monochromatic, there are a lot more glitches with dips of higher absolute amplitude than when the source is polychromatic.
One possible explanation (that might be false) is the following; the channel cut selects one energy at a certain angle but it is not an infinite dirac peak for that single energy, rather it is a (very) narrow gaussian like function, one can look at the rocking curves as an example.
This means that for many of the glitches, it is possible that some rays with an angle(so an energy) close to the energy intended to be selected pass through the CC and hit the monitors, making the glitch of the energy intended to be selected, lower in absolute amplitude or even disapear.   
Only glitches that have quite a few energies close by that are also glitches retain high absolute amplitudes because the energy there is stolen and therefore there is a decrease in rays transmitted to the monitors.  
When the source is monochromatic this problem does not exist because the energy the cc selects is the energy the source sends (modulo a very small dE of 1eV). Therefore the dips in intensity can have high absolute amplitudes for all glitches because only the rays of that energy go and hit the monitor(or don't as much, leading to a glitch).  

Here are two pictures illustrating the above explanation:

###### White light :  

![glitches white source](images/Glitches_Interpretation_white_cropped_2.png)

###### Monochromatic light : 

![glitches monochromatic source](images/Glitches_Interpretation_mono_cropped.png)
